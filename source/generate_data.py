import random
import names
import sys
import csv

try:
    num_of_rows = int(sys.argv[1])
except IndexError:
    num_of_rows = 100

header = ["src", "dst"]

def generate_user():
    header = ["id", "name"]
    data = []
    data.append(header)
    for i in range(num_of_rows):
        tmp = ["p"+str(i+1), names.get_first_name()]
        data.append(tmp)

    csvfile = "../dataset/user.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

def generate_location():
    header = ["id", "name", "latitude", "longitude"]
    data = []
    data.append(header)
    for i in range(num_of_rows):
        rand_a = random.randint(0, num_of_rows)
        rand_b = random.randint(0, num_of_rows)
        tmp = ["l"+str(i+1), names.get_last_name(), rand_a, rand_b]
        data.append(tmp)

    csvfile = "../dataset/location.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

def generate_p_to_p():
    users = []
    data = []
    data.append(header)
    for i in range(num_of_rows):
        u = "p"+str(i+1)
        users.append(u)
    for p in users:
        n_rel = random.randint(0, 20)
        p_to_p = int(0.7 * n_rel)
        for i in range(p_to_p):
            tmp = [p, "p"+str(random.randint(1, num_of_rows))]
            data.append(tmp)
    csvfile = "../dataset/friendship.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

def generate_p_to_l():
    users = []
    data = []
    data.append(header)
    for i in range(num_of_rows):
        u = "p"+str(i+1)
        users.append(u)
    for p in users:
        n_rel = random.randint(0, 20)
        p_to_l = int(0.3 * n_rel)
        for i in range(p_to_l):
            tmp = [p, "l"+str(random.randint(1, num_of_rows))]
            data.append(tmp)
    csvfile = "../dataset/checkin.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

def generate_l_to_l():
    places = []
    data = []
    data.append(header)
    for i in range(num_of_rows):
        u = "l"+str(i+1)
        places.append(u)
    for l in places:
        n_rel = random.randint(0, 5)
        l_to_l = int(0.7 * n_rel)
        for i in range(l_to_l):
            tmp = [l, "l"+str(random.randint(1, num_of_rows))]
            data.append(tmp)
    csvfile = "../dataset/follow.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

def generate_query():
    data = []
    for i in range(20):
        rand_x1 = random.randint(0, num_of_rows)
        rand_y1 = random.randint(0, num_of_rows)
        rand_x2 = random.randint(0, num_of_rows)
        rand_y2 = random.randint(0, num_of_rows)
        tmp = [rand_x1, rand_y1, rand_x2, rand_y2]
        data.append(tmp)
    csvfile = "../dataset/query.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(data)

if __name__ == '__main__':
    generate_user()
    generate_location()
    generate_p_to_p()
    generate_p_to_l()
    generate_l_to_l()
    generate_query()