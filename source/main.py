import csv
import math
import time
import os
import sys
import psutil
from collections import defaultdict
from collections import OrderedDict

class User:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.friends = list()
        self.checkin = list()
        self.rmbr = dict()

    def add_friends(self, user_id):
        self.friends.append(user_id)

    def add_checkin(self, location_id):
        self.checkin.append(location_id)

class Location:
    def __init__(self, id, name, lat, long):
        self.id = id
        self.name = name
        self.lat = lat
        self.long = long
        self.follow = list()
        self.score = dict()
        self.rmbr = {
            'min_x': lat,
            'min_y': long,
            'max_x': lat,
            'max_y': long
        }
    
    def add_score(self, id, score):
        self.score[id] = score

    def add_follow(self, location_id):
        self.follow.append(location_id)

class Graph:
    def __init__(self):
        self.user = list()
        self.location = list()
        self.loc_graph = defaultdict(list)
        self.full_graph = defaultdict(list)

    def insert_user(self, file):
        users = dict()
        with open(file, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                users[row[csv_reader.fieldnames[0]]] = User(row[csv_reader.fieldnames[0]], row[csv_reader.fieldnames[1]])
                self.user.append(users[row[csv_reader.fieldnames[0]]])
    
    def insert_location(self, file):
        places = dict()
        with open(file, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                places[row[csv_reader.fieldnames[0]]] = Location((row[csv_reader.fieldnames[0]]), row[csv_reader.fieldnames[1]], int(row[csv_reader.fieldnames[2]]), int(row[csv_reader.fieldnames[3]]))
                self.location.append(places[row[csv_reader.fieldnames[0]]])
          
    def insert_friends(self, file):
        with open(file, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                for p in self.user:
                    # if id csv = id obj
                    if row[csv_reader.fieldnames[0]] == p.id:
                        # self loop 
                        if row[csv_reader.fieldnames[1]] == p.id:
                            del row[csv_reader.fieldnames[1]]
                        else:
                            # add obj to friend list
                            for i in self.user:
                                if row[csv_reader.fieldnames[1]] == i.id:
                                    p.add_friends(i)                                   
                    # remove redundant value
                    p.friends = list(dict.fromkeys(p.friends))

    def insert_checkin(self, file):
        with open(file, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                for p in self.user:
                    # if id csv = id obj
                    if row[csv_reader.fieldnames[0]] == p.id:
                        # add obj to checkin list
                        for i in self.location:
                            if row[csv_reader.fieldnames[1]] == i.id:
                                p.add_checkin(i)
                    # remove redundant value
                    p.checkin = list(dict.fromkeys(p.checkin))

    def insert_follow(self, file):
        with open(file, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                for l in self.location:
                    # if id csv = id obj
                    if row[csv_reader.fieldnames[0]] == l.id:
                        # self loop 
                        if row[csv_reader.fieldnames[1]] == l.id:
                            del row[csv_reader.fieldnames[1]]
                        else:
                            # add obj to follow list
                            for i in self.location:
                                if row[csv_reader.fieldnames[1]] == i.id:
                                    l.add_follow(i)
                    # remove redundant value                    
                    l.follow = list(dict.fromkeys(l.follow))

    def build_graph(self):
        # location
        for l in self.location:
            if len(l.follow) > 0:
                for i in l.follow:
                    self.loc_graph[l.id].append(i.id)

        # full graph
        for p in self.user:
            if len(p.friends) > 0:
                for f in p.friends:
                    self.full_graph[f.id].append(p.id)
                    # self.full_graph[p.id].append(f.id)
            if len(p.checkin) > 0:
                for l in p.checkin:
                    self.full_graph[l.id].append(p.id)
                    # self.full_graph[p.id].append(l.id)
        for l in self.location:
            if len(l.follow) > 0:
                for i in l.follow:
                    self.full_graph[i.id].append(l.id)
                    # self.full_graph[l.id].append(i.id)

    def update_rmbr(self, obj, loc_path):
        if obj == 'l':
            for path in loc_path.values():
                path.remove(path[-1])
                for i in reversed(path):
                    for l in self.location:
                        if i == l.id:
                            for j in l.follow:
                                if l.rmbr['min_x'] > j.rmbr['min_x']:
                                    l.rmbr['min_x'] = j.rmbr['min_x']
                                if l.rmbr['min_y'] > j.rmbr['min_y']:
                                    l.rmbr['min_y'] = j.rmbr['min_y']
                                if l.rmbr['max_x'] < j.rmbr['max_x']:
                                    l.rmbr['max_x'] = j.rmbr['max_x']
                                if l.rmbr['max_y'] < j.rmbr['max_y']:
                                    l.rmbr['max_y'] = j.rmbr['max_y']
                                    
        if obj == 'p':
            # checkin
            for p in self.user:
                # one checkin
                if len(p.checkin) == 1:
                    for i in p.checkin:
                        p.rmbr['min_x'] = i.rmbr['min_x']
                        p.rmbr['min_y'] = i.rmbr['min_y']
                        p.rmbr['max_x'] = i.rmbr['max_x']
                        p.rmbr['max_y'] = i.rmbr['max_y']
                # more than one checkin
                elif len(p.checkin) > 1:
                    for i in p.checkin:
                        for j in reversed(p.checkin):
                            # compare each location rmbr
                            if i.rmbr['min_x'] >= j.rmbr['min_x']:
                                p.rmbr['min_x'] = j.rmbr['min_x']
                            if i.rmbr['min_y'] >= j.rmbr['min_y']:
                                p.rmbr['min_y'] = j.rmbr['min_y']
                            if i.rmbr['max_x'] <= j.rmbr['max_x']:
                                p.rmbr['max_x'] = j.rmbr['max_x']
                            if i.rmbr['max_y'] <= j.rmbr['max_y']:
                                p.rmbr['max_y'] = j.rmbr['max_y']
            for p in self.user:
                # if rmbr is empty and user has friends
                if bool(p.rmbr) == False and len(p.friends) > 0:
                    # loop through user friends
                    for i in p.friends:
                        for j in reversed(p.friends):
                            # if rmbr is not empty
                            if bool(i.rmbr) != False and bool(j.rmbr) != False:
                                if i.rmbr['min_x'] >= j.rmbr['min_x']:
                                    p.rmbr['min_x'] = j.rmbr['min_x']
                                if i.rmbr['min_y'] >= j.rmbr['min_y']:
                                    p.rmbr['min_y'] = j.rmbr['min_y']
                                if i.rmbr['max_x'] <= j.rmbr['max_x']:
                                    p.rmbr['max_x'] = j.rmbr['max_x']
                                if i.rmbr['max_y'] <= j.rmbr['max_y']:
                                    p.rmbr['max_y'] = j.rmbr['max_y']
            for p in self.user:
                if bool(p.rmbr) != False and len(p.friends) > 0:
                    for i in p.friends:
                        if bool(i.rmbr) != False:
                            if p.rmbr['min_x'] > i.rmbr['min_x']:
                                p.rmbr['min_x'] = i.rmbr['min_x']
                            if p.rmbr['min_y'] > i.rmbr['min_y']:
                                p.rmbr['min_y'] = i.rmbr['min_y']
                            if p.rmbr['max_x'] < i.rmbr['max_x']:
                                p.rmbr['max_x'] = i.rmbr['max_x']
                            if p.rmbr['max_y'] < i.rmbr['max_y']:
                                p.rmbr['max_y'] = i.rmbr['max_y']

    def total_user(self):
        return len(self.user)

    def total_location(self):
        return len(self.location)

def get_path(graph, start, path=[]):
    q = [start]
    while q:
        v = q.pop(0)
        if v not in path:
            path = path + [v]
            q = graph[v] + q
    return path

def get_score(graph, start):
    path = dict()
    q = OrderedDict()
    q[start] = 0
    while q:
        (v, hop) = q.popitem(last=False)
        if not v in path.keys():
            path[v] = hop
            for neighbor in graph[v]:
                if neighbor not in path.keys() and neighbor not in q.keys():
                    # print(neighbor, hop+1)
                    q[neighbor] = hop + 1
    return path

def get_sites(area, location):
    sites = list()
    for l in location:
        # if the given area isn't a point(dot)
        if area['min_x'] != area['max_x'] and area['min_y'] != area['max_y']:
            # location has rmbr area
            if l.rmbr['min_x'] != l.rmbr['max_x'] and l.rmbr['min_y'] != l.rmbr['max_y']:
                if l.rmbr['min_x'] >= area['max_x'] or l.rmbr['max_x'] <= area['min_x']:
                    continue
                if l.rmbr['min_y'] >= area['max_y'] or l.rmbr['max_y'] <= area['min_y']:
                    continue
                sites.append(l)
            # location doesn't have rmbr area (point)
            else:
                if l.rmbr['min_x'] >= area['min_x'] and l.rmbr['max_x'] <= area['max_x']:
                    if l.rmbr['min_y'] >= area['min_y'] and l.rmbr['max_y'] <= area['max_y']:
                        sites.append(l)
        else:
            # if the given point is inside rmbr
            if l.rmbr['min_x'] <= area['min_x'] <= l.rmbr['max_x'] and l.rmbr['min_y'] <= area['min_y'] <= l.rmbr['max_y']:
                sites.append(l)
    return sites

def get_suspect_rmbr(area, graph):
    suspect = list()
    for p in graph.user:
        if bool(p.rmbr) != False:
            if area['min_x'] != area['max_x'] and area['min_y'] != area['max_y']:
                # user has rmbr area
                if p.rmbr['min_x'] != p.rmbr['max_x'] and p.rmbr['min_y'] != p.rmbr['max_y']:
                    if p.rmbr['min_x'] >= area['max_x'] or p.rmbr['max_x'] <= area['min_x']:
                        continue
                    if p.rmbr['min_y'] >= area['max_y'] or p.rmbr['max_y'] <= area['min_y']:
                        continue
                    suspect.append(p)
                # user doesn't have rmbr area (point)
                else:
                    if p.rmbr['min_x'] >= area['min_x'] and p.rmbr['max_x'] <= area['max_x']:
                        if p.rmbr['min_y'] >= area['min_y'] and p.rmbr['max_y'] <= area['max_y']:
                            suspect.append(p)
            else:
                # if the given point is inside rmbr
                if p.rmbr['min_x'] <= area['min_x'] <= p.rmbr['max_x'] and p.rmbr['min_y'] <= area['min_y'] <= p.rmbr['max_y']:
                    suspect.append(p)
    return suspect

def get_suspect_graph(sites, graph):
    data = defaultdict(list)
    for i in sites:
        data[i.id] = get_score(graph.full_graph, i.id)
    for key, value in data.items():
        for i, score in value.items():
            if score > 0:
                for j in range(graph.total_location()):
                    if graph.location[j].id == key:
                        graph.location[j].add_score(i, score)
    return

if __name__ == '__main__':
    # user = input('Masukkan data pengguna: ')
    # location = input('Masukkan data lokasi: ')
    # follow  = input('Masukkan data follow: ')
    # friend = input('Masukkan data relasi: ')
    # checkin = input('Masukkan data checkin: ')

    user = '../dataset/user.csv'
    location = '../dataset/location.csv'
    follow = '../dataset/follow.csv'
    friend = '../dataset/friendship.csv'
    checkin = '../dataset/checkin.csv'

    ts = time.time()

    graph = Graph()
    graph.insert_user(user)
    graph.insert_location(location)
    graph.insert_follow(follow)
    graph.insert_friends(friend)
    graph.insert_checkin(checkin)
    graph.build_graph()

    print("\nData berhasil dimasukkan")

    # for updating location rmbr
    loc_dfs = defaultdict(list)
    for i in list(graph.loc_graph):
        loc_dfs[i] = get_path(graph.loc_graph, i)
    
    graph.update_rmbr('l', loc_dfs)
    graph.update_rmbr('p', loc_dfs)

    precomputing = time.time() - ts
    # print(precomputing)

    opt = input('\nEnter type: ')
    while(opt != 'stop'):

        print('\n----------- Masukkan Area -----------')
        area = dict()
        area['min_x'] = int(input('Enter min_x: '))
        area['min_y'] = int(input('Enter min_y: '))
        area['max_x'] = int(input('Enter max_x: '))
        area['max_y'] = int(input('Enter max_y: '))
        
        # MBR BASED SOLUTION
        if opt == "rmbr":
            start = time.time()
            suspect = get_suspect_rmbr(area, graph)
            print('\nJumlah Pengguna: ' + str(len(suspect)))

            # LOG
            runtime = time.time() - start
            process = psutil.Process(os.getpid())
            mem_usage = process.memory_info().rss

            csvlog = "log_rmbr.csv"
            with open(csvlog, "a") as output:
                writer = csv.writer(output, lineterminator='\n', quoting = csv.QUOTE_MINIMAL)
                writer.writerow([precomputing, runtime, mem_usage])

            # OUTPUT
            hasil_rmbr = "hasil_rmbr.csv"
            with open(hasil_rmbr, "w") as output:
                writer = csv.writer(output, lineterminator='\n', quoting = csv.QUOTE_MINIMAL)
                writer.writerow(["user id", "name"])
                for i in suspect:
                    writer.writerow([i.id, i.name])
            
            print("\nHasil dapat dilihat di " + hasil_rmbr)
            print('-------------------------------------')

        # GRAPH TRAVERSAL BASED SOLUTION
        if opt == "graf":
            start = time.time()
            ts_graph = time.time()
            
            sites = get_sites(area, graph.location)
            # print(len(sites))
            get_suspect_graph(sites, graph)

            # LOG
            runtime = time.time() - start
            process = psutil.Process(os.getpid())
            mem_usage = process.memory_info().rss

            csvlog = "log_graf.csv"
            with open(csvlog, "a") as output:
                writer = csv.writer(output, lineterminator='\n', quoting = csv.QUOTE_MINIMAL)
                writer.writerow([precomputing, runtime, mem_usage])

            hasil_graf = "hasil_graf.csv"
            with open(hasil_graf, "w") as output:
                writer = csv.writer(output, lineterminator='\n', quoting = csv.QUOTE_MINIMAL)
                writer.writerow(["destination", "user id", "name", "hop"])
                for i in graph.location:
                    if len(i.score) > 0:
                        for j, score in i.score.items():
                            if j[0] != 'l':
                                for k in graph.user:
                                    if j == k.id:
                                        writer.writerow([i.id, k.id, k.name, score])
            
            print("\nHasil dapat dilihat di " + hasil_graf)
            print('-------------------------------------')

        opt = input('\nEnter type: ')
